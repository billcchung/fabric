# Modified from Splunkit Data Generator Script

from time import time, gmtime, strftime, clock, sleep
from os import path
import shutil
import stopwatch
import splunk.Intersplunk as sis
(a, kwargs) = sis.getKeywordsAndOptions()
in_file = path.join(path.dirname(path.realpath(__file__)), 'sample.log')
out_file = path.join(sis.splunkHome(), 'var', 'spool', 'splunk', 'gendata.log')

# by default, this script will generate 10000 events (lowest stop condition)
default = {'in_file': in_file, 
           'out_file': out_file,
           'size': 100,       # MB
           'rate': -1,        # GB/hour
           'run_time': -1.0,  # mins
           'events': 10000,  
           'earliest': 86400, # seconds ago
           'eps': 10          # generate # events in a second of timestamp
           }

args = {}
if kwargs:
    for key in default.keys():
        if key in kwargs: # if user set any arg, apply it and set others as -1
            args.update({key:kwargs[key]})
        elif key == 'out_file' or key == 'in_file' :
            args.update({key:default[key]})
        else:
            args.update({key:-1})
else:
    args = default

def main():

    in_file   = args['in_file']
    out_file  = args['out_file']
    rate     = float(args['rate'])
    run_time = int(args['run_time'])
    events   = int(args['events'])
    size     = int(args['size'])
    earliest = int(args['earliest'])
    eps      = int(args['eps'])
    bytes_written = 0
    bytes_per_second_max = (rate * 1024 * 1024) / 3600.0
    bytes_this_second = 0
    max_bytes = size * 1024 * 1024
    line_num = 1

    # events starting from earliest (in days)
    time_subtract = earliest
    # subtract secs in day from current epoch time
    timestamp = time() - time_subtract
    count = 1
    timer = stopwatch.Timer()
    last_sleep = clock()

    out_file = open(out_file, 'a+')
    in_file = open(in_file, 'rU')
    while(True):

        if ( ((run_time != -1.0) and ((timer.elapsed / 60) >= run_time)) or
             ((size != -1) and (bytes_written >= max_bytes)) or 
             ((events != -1) and (line_num > events))):
            break;

        in_file.seek(0)

        for in_line in in_file:

            if ( ((run_time != -1.0) and ((timer.elapsed / 60) >= run_time)) or
                 ((size != -1) and (bytes_written >= max_bytes)) or
                 ((events != -1) and (line_num > events))):
                break;

            if (count > eps): # i.e. set timestamp to next sec while reached eps.
                timestamp += 1
                count = 1

            out_line =  strftime('%Y-%m-%d %H:%M:%S %Z', gmtime(timestamp)) +\
                       " lline_num=%09d " % (line_num) + \
                       in_line.rstrip("\n") + \
                       " rline_num=%09d\n" % (line_num)
            out_file.write(out_line)

            if ((rate != -1) and (bytes_this_second > bytes_per_second_max)):
                write_time = clock() - last_sleep
                sleep_time = 1.0 - write_time
                if (sleep_time > 0):
                    sleep(sleep_time)
                last_sleep = clock()
                bytes_this_second = 0

            count+=1
            line_num += 1
            line_length = len(out_line)
            bytes_this_second += line_length
            bytes_written += line_length

    out_file.close()
    in_file.close()
    sis.outputResults([{"Events generated": line_num-1}])


try:
    main()
except Exception, e:
    import traceback
    stack =  traceback.format_exc()
    si.generateErrorResults("Error '%s'. %s" % (e, stack))   