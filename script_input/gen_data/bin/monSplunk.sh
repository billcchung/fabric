#! /bin/bash
# Bill Chung <bchung@splunk.com>
# July, 2013

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=`dirname "$SCRIPT"`
PAGESIZE=`getconf PAGESIZE`
HOST=`hostname -s`

splunkHome=$SPLUNK_HOME

infoFile="$SCRIPTPATH/monSplunk_info.log"
splunkdPidFile="$splunkHome/var/run/splunk/splunkd.pid"
webPidFile="$splunkHome/var/run/splunk/splunkweb.pid"

# check args
if [ ! -d "$splunkHome" ]; then
    printf "SPLUNK_HOME=$splunkHome doesnt exist, quit script!\n" >> $infoFile
    printf "Usage: $SCRIPT SPLUNK_HOME [INTERVAL]\n" >> $infoFile
    exit 1
elif [ ! -f "$splunkHome/etc/splunk.version" ]; then
    printf "splunk.version file is missing, incomplete tarball?\n" >> $infoFile
    exit 2
else
    splunkInfo=`awk '/VERSION|BUILD|PLATFORM/{printf "%s, ", tolower($1)}' $splunkHome/etc/splunk.version`
fi
printf "[`date`] Start monitoring splunk, SPLUNK_HOME=$1\n" >> $infoFile

function getPerf() {
    local pidFile=$1 webPidFile=$2
    local splunkdPid=`head -n 1 $pidFile`
    local splunkwebPid=`head -n 1 $webPidFile`
    for pid in `cat $pidFile $webPidFile`; do
        if [ -f /proc/$pid/stat ]; then 
            # the results will have 8 items (time millisCPU threads resMemB virtMemB pageFaults readsB writesB)
            declare "before_$pid=`date +%s.%N` `awk '{printf "%.0f %d %.0f %.0f %.0f", $14+$15+$16+$17, $20, $24*pagesize, $23, $10+$12}' pagesize=$PAGESIZE /proc/$pid/stat` `awk '/char/{printf " %.0f", $2}' /proc/$pid/io`"
        else
            printf "[`date`] Before: process=$pid doenst exist anymore, presume terminated.\n" >> $infoFile
        fi 
    done

    sleep 1

    for pid in `cat $pidFile $webPidFile`; do
        isBefore="before_$pid"
        if [[ -f /proc/$pid/stat ]] && [[ "${!isBefore}" ]]; then # exists in the recording above.

            declare "after_$pid=`date +%s.%N` `awk '{printf "%.0f %d %.0f %.0f %.0f", $14+$15+$16+$17, $20, $24*pagesize, $23, $10+$12}' pagesize=$PAGESIZE /proc/$pid/stat` `awk '/char/{printf " %.0f", $2}' /proc/$pid/io`"
            b_ref="before_$pid"
            a_ref="after_$pid"
            # convert the results to an array
            before=( ${!b_ref} )
            after=( ${!a_ref} )

            # skip if the array doesnt have correct item count.
            if [[ "${#after[*]}" != "8" ]] || [[ "${#before[*]}" != "8" ]]; then 
                printf "[`date`] process=$pid does have correct items, before=${before[*]}, after=${after[*]}\n" >> $infoFile
                continue 
            fi
            # get process name
            if [ "$pid" == "$splunkdPid" ]; then
                pname="splunkd"
            elif [ "$pid" == "$splunkwebPid" ]; then
                pname="splunkweb"
            else
                pname="`ps -p $pid -o cmd=| awk '/search/{printf "search"} /optimize/{printf "optimize"} /btool/{printf "btool"} /gzip/{printf "gzip"} /process-runner/{printf "process-runner"} /monSplunk/{printf "monSplunk"}'`"
            fi

            timeDelta=$(   echo "( ${after[0]} - ${before[0]} )" | bc )
            millisCPU=$(   echo "( ${after[1]} - ${before[1]} )*1000/100" | bc )
            threads=$(     echo "( ${after[2]} + ${before[2]} )/2" | bc )
            resMemMB=$(    echo "( ${after[3]} + ${before[3]} )/1024/1024/2" | bc )
            virtMemMB=$(   echo "( ${after[4]} + ${before[4]} )/1024/1024/2" | bc )
            pageFaultsPS=$(echo "( ${after[5]} - ${before[5]} )/$timeDelta" | bc )
            readsKBPS=$(   echo "( ${after[6]} - ${before[6]} )/1024/$timeDelta" | bc )
            writesKBPS=$(  echo "( ${after[7]} - ${before[7]} )/1024/$timeDelta" | bc )
            totalCPUTime=${after[1]*10}
            totalPageFaults=${after[5]}
            totalReadsKB=$( echo "${after[6]}/1024" | bc )
            totalWritesKB=$(echo "${after[7]}/1024" | bc )
            timeNow=`date "+%Y-%m-%d %T %Z"`

            printf "$timeNow host=$HOST, ${splunkInfo}pid=$pid, pname=$pname, " 
            printf "millisCPU=$millisCPU, totalCPUTime=$totalCPUTime, " 
            printf "threads=$threads, resMemMB=$resMemMB, virtMemMB=$virtMemMB, " 
            printf "pageFaultsPS=$pageFaultsPS, totalPageFaults=$totalPageFaults, " 
            printf "readsKBPS=$readsKBPS, totalReadsKB=$totalReadsKB, " 
            printf "writesKBPS=$writesKBPS, totalWritesKB=$totalWritesKB\n" 
        else
            printf "[`date`] After: process=$pid doenst exist in before or terminated in after, skipped.\n" >> $infoFile
        fi
    done
}

# Monitoring
if [ -f "$splunkdPidFile" ]; then
    getPerf $splunkdPidFile $webPidFile
else
    printf "[`date`] splunkd.pid doesnt exist, presume splunk not running, quit scirpt!\n" >> $infoFile
    exit 3
fi

printf "[`date`] Finished $0\n" >> $infoFile
exit 0


