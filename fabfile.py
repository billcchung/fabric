from os import path
import json
from fabric.api import env
from fabric.state import output

properties_json = path.join(path.dirname(path.realpath(__file__)),
                           'properties.json')
properties = json.load(open(properties_json))

env.debug = properties['fab_outputs']['debug']
env.verbose = env.debug if env.debug else properties['fab_env']['verbose']

# Modify fab env
for fab_env in properties['fab_env']:
    env.update({fab_env:properties['fab_env'][fab_env]})

# Modify fab output level
for key in output.keys():
    output[key] = properties['fab_outputs'][key]

try:
    from lib.cluster import *
    from lib.splunk import *
    from lib.dist import *
except ImportError, ex:
    print ex


