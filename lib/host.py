import sys
import yaml
import inspect
import logging
import urllib
import json
import time
from os import path
# from time import time.ctime
from fabric.api import local, run, put, get, cd, env, roles, parallel
from fabric.colors import red, cyan, magenta, yellow

# prase environments
fab_home_path = path.join(path.dirname(path.realpath(__file__)), '..')
hosts_json = path.join(fab_home_path, 'hosts.json')
# hosts_arg = yaml.load(open(hosts_yml))
hosts_arg = json.load(open(hosts_json))

for node in hosts_arg['nodes']:
    if type(node['host']) == list:
        env.roledefs.update({node['role']:node['host']})
    elif type(node['host']) == str or unicode:
        env.roledefs.update({node['role']:[node['host']]})
    else:
        raise Exception("[ERROR] Host needs to be list or str! Error on host %s"
                        %node['host'])

# Role is specified in sys.argv?
if "-R" in sys.argv:
    run_roles = sys.argv[sys.argv.index("-R")+1]
else:
    run_roles = []
    for node in hosts_arg['nodes']:
        if node['role'] == 'cluster_master' or node['role'] == 'indexer':
            # so master or indexer will always be the first to setup.
            run_roles.insert(0, node['role'])
        else:
            run_roles.append(node['role'])

if env.debug:
    print(magenta("[DEBUG] roledefs: %s"  %(env.roledefs)))
    print(magenta("[DEBUG] run_roles: %s" %(run_roles)))


def host_args_parser(host):
    '''
    Parse the host arguements as dict.
    Host argeuments will overwrite the defaults,
    it'll find the corresponding positions if it was defined as a list.
    @param: host (env.host_string)
    '''
    args = {}

    # find the position(index) and values for a host.
    for node in hosts_arg['nodes']:
        if host in node['host']: # found what role this host belongs to
            pos = node['host'].index(host) # find its position
            for key in hosts_arg['host_params']:
                if key in node: # if key is defined in role-level
                    if type(node[key]) == list: # for a specific host
                        args.update({key:node[key][pos]})
                    elif type(node[key]) == str or int or unicode: # all hosts of a role
                        args.update({key:node[key]})
                    else:
                        raise Exception("Argument should be list, str, or int!")
                else: # not defined in role-level, apply default value
                    args.update({key:hosts_arg['default_params'][key]})
        else: # host is not in this node/role
            continue # not necessary, just to be clearer.

    if env.debug:
        print(magenta("[DEBUG] Host args of %s (called from %s):\n%s" %(
               host, inspect.stack()[1][3], args)))
        print(magenta("[DEBUG] Cehcking args are valid."))

    # additional arguements
    if args['is_fetch_pkg']:
        if 'splunk_build_fetcher.py' in args['pkg_url']:
            args['pkg_url'] = urllib.urlopen(
                args['pkg_url']).readline().strip()
        args.update({'pkg': path.basename(args['pkg_url'])})
    elif args['is_send_pkg']:
        args.update({'pkg': path.basename(args['pkg_path'])})
    else:
        args.update({'pkg': path.basename(args['pkg_path'])})

    if args['pkg'].startswith('splunkforwarder'):
        if args['pkg'].endswith('.msi'):
            basepath = 'SplunkUniversalForwarder'
            product = 'UniversalForwarder'
        else:
            product = basepath = 'splunkforwarder'
    else:
        if args['pkg'].endswith('.msi'):
            product = basepath = 'Splunk'
        else:
            product = basepath = 'splunk'
    args.update({'product': product, 'basepath': basepath})

    # dir
    if args['platform'].lower() == 'windows' and args['pkg'].endswith('.msi'):
        splunk_dir   = path.join('/cygdrive/c/Program\ Files', args['basepath'])
    else:
        splunk_dir   = path.join(args['deploy_dir'], args['basepath'])
    app_dir          = path.join(splunk_dir, 'etc', 'apps')
    system_local_dir = path.join(splunk_dir, 'etc', 'system', 'local')

    # conf
    web_conf     = path.join(system_local_dir, 'web.conf')
    server_conf  = path.join(system_local_dir, 'server.conf')
    indexes_conf = path.join(system_local_dir, 'indexes.conf')


    if '@' in args['host']:
        user, hostname = args['host'].split("@")
    else:
        user, hostname = '', args['host']
    args.update({'splunk_dir':splunk_dir,
                 'web_conf': web_conf,
                 'server_conf': server_conf,
                 'indexes_conf': indexes_conf,
                 'app_dir': app_dir,
                 'user': user,
                 'hostname':hostname})
    return args


def _path_join(platform, *args):
    return '\\'.join(args) if platform.lower() == 'windows' else '/'.join(args)


@parallel
@roles(run_roles)
def cmd(exec_cmd=None, directory=''):
    '''
    Issue a command to remote shell.
    @param: cmd, issue directory.
    '''
    if directory: # play safe: create dir if it doesnt exist.
        cmd("mkdir -p %s" %(directory))
    with cd(directory):
        if env.verbose:
            print(cyan("[%s] In [%s:%s] exec_cmd [%s]" %(
                   time.ctime(), env.host_string, directory, exec_cmd)))
        run(exec_cmd)


@parallel
@roles(run_roles)
def put_file(local, remote):
    '''
    Put file or dir to remote host.
    @param: local path, remote path
    '''
    if env.verbose:
        print(cyan("[%s] Put [%s] to [%s:%s]" %(
                   time.ctime(), local, env.host_string, remote)))
    put(local, remote)


@parallel
@roles(run_roles)
def get_file(remote, local):
    '''
    Get file or dir to remote host.
    The file name will append the hostname to its end, to aviod conflicts.
    @param: remote path, local path

    '''
    local_path = path.join(local, path.basename(remote)+'_'+env.host_string)
    if env.verbose:
        print(cyan("[%s] Get [%s] from [%s:%s]" %(
                   time.ctime(), local_path, env.host_string, remote)))
    get(remote, local_path)


@parallel
@roles(run_roles)
def _log(msg, level='info'):
    '''
    ! LOGGING DOESNT WORK IN FAB ! Log a message to log/env.host.
    @param: message, log level
    '''
    log_file = path.join('.', 'log', env.host_string)
    logging.basicConfig(format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        filename=log_file,
                        level=logging.DEBUG)
    logger = getattr(logging, level)
    logger(msg)


@roles(run_roles)
def printenv(fab_env=False, host_env=True):
    '''
    print env, use for debugging.
    @param: fab_env (bool), host_env (bool)
    '''
    print(yellow('========  ' + env.host_string + '  ========'))

    if fab_env in [1, True, 'True', 'Yes', 'T', 'Y']:
        print(magenta("fab env:"))
        for key in env.keys():
            print(cyan("%s:" %key)+" %s; " %env[key]),
        print '\n'

    if host_env in [1, True, 'True', 'Yes', 'T', 'Y']:
        print(magenta("host_args:"))
        host_args = host_args_parser(env.host_string)
        for key in host_args:
            print(cyan("%s:" %key)+" %s; " %host_args[key]),
        print '\n'




