from os import path
import sys
from fabric.contrib.console import confirm
from fabric.api import local, run, cd, env, roles, execute, parallel
from fabric.operations import put
# import module
from host import *
from splunk import *


@parallel
@roles('searchhead')
def setup_searchhead():
    '''
    Setup searchhead node
    @param:
    @host_args: search_server
    '''
    host_args = host_args_parser(env.host_string)
    deploy_splunk()
    for s_server in host_args['search_server'].split(','):
        if len(s_server.split(':')) == 2:
            [s_host, s_port] = [s.strip() for s in s_server.split(':')]
            [s_user, s_pass] = ['admin', 'changeme']
        elif len(s_server.split(':')) == 4:
            [s_host, s_port, s_user, s_pass] = s_server.split(':')
        else:
            print(red("[ERROR] Wrong format of search_server: %s, skipping." %(
                      s_server)))
            continue
        s_ep = 'services/search/distributed/peers'
        s_args = "-d name=%s:%s -d remoteUsername=%s -d remotePassword=%s" %(
                  s_host, s_port, s_user, s_pass)
        splunk_rest(s_ep, s_args, True)


@parallel
@roles('indexer')
def setup_indexer():
    '''
    Setup indexer node
    @param:
    @host_args: listen_type_port, is_with_data, data_type, listen_type_port,
                index, maxTotalDataSizeMB, maxWarmDBCount, maxDataSize
    '''
    host_args = host_args_parser(env.host_string)
    deploy_splunk()
    listen(host_args['listen_type_port'])

    # set the index size, warm db size, and hot db size.
    idx_ep = "servicesNS/nobody/system/data/indexes/{index}".format(**host_args)
    ep_args = "-d maxTotalDataSizeMB={maxTotalDataSizeMB} " \
              "-d maxWarmDBCount={maxWarmDBCount} " \
              "-d maxDataSize={maxDataSize}".format(**host_args)
    splunk_rest(idx_ep, ep_args, True)

    # should this instance comes with data/gen_data?
    if host_args['is_with_data']:
        with_data(host_args['data_type'])


@parallel
@roles('forwarder')
def setup_forwarder():
    '''
    Setup fowarder node
    @param:
    @host_args: forward_server, is_with_data, static_data, splunk_dir,
                listen_type_port
    '''
    host_args = host_args_parser(env.host_string)
    deploy_splunk()
    listen(host_args['listen_type_port'])

    f_server_ep = 'services/data/outputs/tcp/server'
    for f_server in host_args['forward_server'].split(','):
        ep_args = "-d name=%s" %(f_server.strip())
        splunk_rest(f_server_ep, ep_args, True)
        #splunk_cmd('add forward-server %s' %(f_server.strip()))
    if host_args['use_ack']:
        use_ack_cmd = (r"find . -wholename *local/outputs.conf -exec sed -i "
                       r"'1s/^/[tcpout]\nuseACK=true\n/' {} \;")
        cmd(use_ack_cmd, host_args['splunk_dir'])

    if host_args['is_with_data']:
        with_data(host_args['data_type'])

