import random
import string
import time
from os import path
from fabric.api import local, run, cd, roles, parallel, execute, runs_once, task
from fabric.colors import red, cyan, magenta, yellow


# import module
from host import *

ep_splunk = {
    'restart': '/services/server/control/restart',
    'index': 'servicesNS/nobody/system/data/indexes/{index}'}

@parallel
@roles(run_roles)
def splunk_cmd(exec_cmd=None, quiet=False, auth=False):
    '''
    Execute a splunk command at splunk_dir.
    @param: exec_cmd
    '''
    host_args = host_args_parser(env.host_string)
    exec_cmd = path.join('bin', 'splunk')+' '+exec_cmd
    if any(x in exec_cmd for x in ['start', 'restart']):
        exec_cmd += ' --accept-license --no-prompt --answer-yes'
    elif auth:
        exec_cmd += " -auth {auth}".format(**host_args)
    else:
        pass

    if quiet:
        exec_cmd += ' >> splunk_cmd.log 2>&1 '

    with cd(host_args['splunk_dir']):
        if env.verbose:
            print(cyan("[%s] In [%s:%s] run splunk_cmd [%s]" %(time.ctime(),
                  host_args['host'], host_args['splunk_dir'], exec_cmd)))
        run(exec_cmd)


@parallel
@roles(run_roles)
def splunk_rest(endpoint, args=None, quiet=False):
    '''
    Execute a rest call to remote form local shell.
    @param: endpoint, args, slient
    '''
    host_args = host_args_parser(env.host_string)
    curl_cmd = ("curl -s -k -u {auth} "
                "https://{hostname}:{splunkd_port}/".format(**host_args) +
                str(endpoint))
    if args:
        curl_cmd += ' ' + args
    if quiet:
        curl_cmd += ' >> splunk_rest.log 2>&1'
    if env.verbose:
        print(cyan("[%s] Locally run splunk_rest [%s]" %(
                    time.ctime(), curl_cmd)))
    local(curl_cmd)


@parallel
@roles(run_roles)
def deploy_splunk():
    '''
    Deploy splunk pkg, set ports, and start splunk.
    @param:
    @host_args: is_backup, is_upgrade, is_send_pkg, pkg, deploy_dir, platform,
    '''
    host_args = host_args_parser(env.host_string)
    if host_args['is_backup']:
        bk_cmd = ("if [[ -d splunk_5 ]]; then "
                  "  splunk_5/bin/splunk stop > /dev/null; "
                  "  rm -rf splunk_5; fi; "
                  "for i in `seq 4 1`; do "
                  "  if [[ -d splunk_$i ]]; then "
                  "    splunk_$i/bin/splunk stop > /dev/null; "
                  "    mv splunk_$i splunk_$((i+1)); fi; "
                  "done; cp -r splunk splunk_1")
        # bk_cmd = "mv splunk splunk_1"
        cmd(bk_cmd, host_args['deploy_dir'])

    if host_args['is_upgrade']:
        stop_cmd = ("if [[ -d splunk ]]; then "
                    "  splunk/bin/splunk stop > /dev/null; fi")
        cmd(stop_cmd, host_args['deploy_dir'])
    else:
        remove_splunk()

    pkg = host_args['pkg']

    # fetch/send pkg, fetch takes priority
    if host_args['is_fetch_pkg']:
        cmd("wget -q -nc {pkg_url}".format(**host_args),host_args['deploy_dir'])
    elif host_args['is_send_pkg']:
        put(host_args['pkg'], host_args['deploy_dir'])
    else: # assume the pkg is there already.
        print(yellow("Warning: no fetching/sending pkg, using pkg_path "
                     "'{pkg_path}' as default pkg, and "
                     "assume it's at remote host already".format(**host_args)))


    # check pkg to determine how to install.
    if pkg.endswith('.tgz'):
        install = 'tar -xf %s' %(pkg)
    elif pkg.endswith('.zip'):
        install = ('unzip -q %s; cmd /C "icacls splunk /T /C /grant '
                   'Administrators:f" | grep files' %(pkg))
    elif pkg.endswith('.Z'):
        install = 'tar -zxf %s' %(pkg)
    elif pkg.endswith('.msi'):
        install = ("taskkill /IM msiexec.exe /F; msiexec /i {pkg} "
                   "LAUNCHSPLUNK=0 AGREETOLICENSE=Yes ".format(**host_args))
        if host_args['logon_username'] and host_args['logon_password']:
            install += ("LOGON_USERNAME={logon_username} "
                        "LOGON_PASSWORD={logon_password}".format(**host_args))
        install += " /quiet"
    else:
        print(red("[ERROR] Unable to handle this pkg %s" %(pkg)))
        raise Exception("[ERROR] Unable to handle pkg %s, not supported."%(pkg))
    cmd(install, host_args['deploy_dir'])

    set_port = ("printf '\n[settings]\n"
                        "mgmtHostPort = 127.0.0.1:{splunkd_port}\n"
                        "httpport = {splunkweb_port}\n"
                        "appServerPorts = {appserver_port}'"
                " >> {web_conf}".format(**host_args))
    cmd(set_port)
    rlogin_kvport = ("printf '\n[general]\nallowRemoteLogin=always\n"
                               "[kvstore]\nport={kvstore_port}'"
                     " >> {server_conf}".format(**host_args))
    cmd(rlogin_kvport)

    # if not host_args['is_upgrade']:
    #     char_set = string.ascii_uppercase + string.digits
    #     server_name = ''.join(random.sample(char_set*6,6))
    #     ch_name_cmd = "printf '\n[general]\nserverName=%s_%s\n' >> %s" %(
    #                   host_args['hostname'], server_name.replace('.', '-'),
    #                   host_args['server_conf'])
    #     cmd(ch_name_cmd)

    splunk_cmd('start', True)


@parallel
@roles(run_roles)
def monitor(local_data, remote_dir='data'):
    '''
    Send a local data to remote dir (under deploy_dir) and monitor it.
    @param: local_data, remote_dir (default to 'data')
    @host_args: deploy_dir
    '''
    host_args = host_args_parser(env.host_string)
    remote_dir = path.join(host_args['deploy_dir'], remote_dir)
    run("mkdir -p %s" %(remote_dir))
    put(local_data, remote_dir)
    remote_data = path.join(remote_dir, path.basename(local_data))
    splunk_cmd("add monitor %s" %remote_data)


@parallel
@roles(run_roles)
def listen(listen_type_port):
    '''
    Listen a port, in 'type:port' format, e.g. 'splunktcp:9996'.
    @param: listen_type_port (in 'type:port' format)
    @host_args:
    '''
    host_args = host_args_parser(env.host_string)

    for listen in listen_type_port.split(','):
        if len(listen.split(':')) == 2:
            [listen_type, listen_port] = [l.strip() for l in listen.split(':')]
            if listen_type == 'splunktcp':
                ep = 'servicesNS/nobody/search/data/inputs/tcp/cooked'
                ep_args = '-d name=%s' %(listen_port)
            elif listen_type == 'tcp':
                ep = 'servicesNS/nobody/search/data/inputs/tcp/raw'
                ep_args = '-d name=%s' %(listen_port)
            elif listen_type == 'udp':
                ep = 'servicesNS/nobody/search/data/inputs/udp'
                ep_args = '-d name=%s' %(listen_port)
            else:
                print(red("[ERROR] Not supported type %s, skipping" %(
                           listen_type)))
                continue
        else:
            print(red("[ERROR] Wrong listen format %s, skipping" %(
                       listen_type_port)))
            continue
        splunk_rest(ep, ep_args, True)

@parallel
@roles(run_roles)
def with_data(data_type):
    '''
    Add specified data (data_file/scirpt) to splunk.
    Supply data_type, and it'll add data_file or script_input_dir to splunk.
    @param: data_type
    @host_args: script_input_dir, app_dir or static_file

    '''
    host_args = host_args_parser(env.host_string)

    if data_type == 'script_input':
        put(host_args['script_input_dir'], host_args['app_dir'])
        cmd('chmod +x gen_data/bin/gendata.sh gen_data/bin/monSplunk.sh',
            host_args['app_dir'])
        sed_cmd = ("sed -i '/eps/ s/=.*$/={events_per_second}/'"
                   " gen_data/bin/gendata.sh; ".format(**host_args))
        cmd(sed_cmd, host_args['app_dir'])
        splunk_cmd('restart', True)
    elif data_type == 'static_data':
        monitor(host_args['static_data'])
    else:
        print(red("[ERROR] Not supported data type %s" %(data_type)))


@parallel
@roles(run_roles)
def put_splunk_file(local, remote):
    '''
    Put file (dir) into remote splunk instance (under $deploy_dir/splunk)
    @params: local path, remote path
    @host_args:
    '''
    host_args = host_args_parser(env.host_string)
    remote_path = path.join(host_args['splunk_dir'], remote)
    put_file(local_path, remote_path)


@parallel
@roles(run_roles)
def get_splunk_file(remote, local):
    '''
    Get file (dir) from remote splunk instance (under $deploy_dir/splunk).
    The file name will append the hostname to its end, to aviod conflicts.
    @params: remote path, local path
    @host_args:
    '''
    host_args = host_args_parser(env.host_string)
    remote_path = path.join(host_args['splunk_dir'], remote)
    get_file(remote_path, local)


# Dont set roles here, let 'fabric.api.execute' to handle which role to run.
@runs_once
def setup():
    '''
    Setup all nodes.
    Use execute to run the setup for certain role.
    @param:
    @host_args:
    '''
    if env.debug:
        print(magenta("[DEBUG] run_roles @setup: %s" %(run_roles)))

    for role in run_roles:
        print(yellow("#### [%s] Setting up %s ####" %(
                      time.ctime(), role), True))
        setup_function = 'setup_'+role
        execute(setup_function) # execute will apply its role!


@parallel
@roles(run_roles)
def remove_splunk():
    '''
    Remove splunk instance on remote hosts (the instance specified in yml).
    @param:
    @host_args: platform, splunk_dir, deploy_dir
    '''
    host_args = host_args_parser(env.host_string)
    if env.verbose:
        print(cyan("[%s] Removing splunk on host %s:%s" %(
              time.ctime(), env.host_string, host_args['splunk_dir'])))
    splunk_cmd('stop', True)
    if host_args['platform'].lower() == 'windows':
        if host_args['pkg'].endswith('.zip'):
            splunk_cmd('disable boot-start')
        elif host_args['pkg'].endswith('.msi'):
            cmd("echo '' | wmic /NODE:localhost product where "
                "'name=\"{product}\"' Call uninstall".format(**host_args))
    else:
        rm_cmd = "rm -rf %s" %(host_args['splunk_dir'])
        cmd(rm_cmd, host_args['deploy_dir'])


# @parallel
# @roles(run_roles)
# def install_splunk():
#     pass


@parallel
@roles(run_roles)
def clean_deploy_dir():
    '''
    Clean all data in deploy_dir (including splunk instances).
    @param:
    @host_args: deploy_dir
    '''
    host_args = host_args_parser(env.host_string)
    if env.verbose:
        print(cyan("[%s] Clean deploy_dir on host %s" %(
                    time.ctime(), env.host_string)))
    rm_cmd = ("for i in `ls`; do "
              "  if [[ -d $i ]]; then "
              "    $i/bin/splunk stop > /dev/null; fi;"
              "done; rm -rf *")
    cmd(rm_cmd, host_args['deploy_dir'])


@parallel
@roles(run_roles)
def add_stanza(conf, stanza, key, value):
    '''
    !NOT IMPLEMENTED YET! Add a stanza/key/value to a conf.
    @param: conf, stanza, key, value
    @host_args:
    '''
    pass
