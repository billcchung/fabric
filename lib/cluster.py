import time

from fabric.contrib.console import confirm
from fabric.api import local, run, cd, env, roles, execute, parallel
from fabric.operations import put
# import modules
from host import *
from splunk import *

ep_cluster = {
    'config': 'services/cluster/config/config'}


@parallel
@roles('cluster_master')
def setup_cluster_master():
    '''
    Setup cluster master node.
    @param:
    @host_args: replication_factor, search_factor, auth
    '''
    host_args = host_args_parser(env.host_string)
    deploy_splunk()
    ep_args = ("-d mode=master "
               "-d replication_factor={replication_factor} "
               "-d search_factor={search_factor}".format(**host_args))
    splunk_rest(ep_cluster['config'], ep_args, True)
    splunk_cmd('restart', True)


@parallel
@roles('cluster_searchhead')
def setup_cluster_searchhead():
    '''
    Setup cluster searchhead node.
    @param:
    @host_args: master_uri, auth
    '''
    host_args = host_args_parser(env.host_string)
    deploy_splunk()
    ep_args = ("-d mode=searchhead "
               "-d master_uri=https://{master_uri}".format(**host_args))
    splunk_rest(ep_cluster['config'], ep_args, True)
    splunk_cmd('restart', True)


@parallel
@roles('cluster_slave')
def setup_cluster_slave():
    '''
    Setup cluster slave (peer) node.
    @param:
    @host_args: master_uri, replication_port, auth, is_with_data, data_type,
                listen_type_port, index, maxTotalDataSizeMB, maxWarmDBCount,
                maxDataSize
    '''
    host_args = host_args_parser(env.host_string)
    deploy_splunk()
    ep_args = ("-d mode=slave "
               "-d master_uri=https://{master_uri} "
               "-d replication_port={replication_port}".format(**host_args))
    time.sleep(10) # give some time for splunk to start/migrate
    splunk_rest(ep_cluster['config'], ep_args, True)
    listen(host_args['listen_type_port'])

    # set the index size, warm db size, and hot db size.
    ep_args = ("-d maxTotalDataSizeMB={maxTotalDataSizeMB} "
               "-d maxWarmDBCount={maxWarmDBCount} "
               "-d maxDataSize={maxDataSize}".format(**host_args))
    splunk_rest(ep_splunk['index'].format(**host_args), ep_args, True)
    # should this instance come with data/gen_data?
    if host_args['is_with_data']:
        with_data(host_args['data_type'])
    splunk_cmd('restart', True)


